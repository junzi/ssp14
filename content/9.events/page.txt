title: Public Events
-
date: Feb 2014
-
content: 

### Rocket Launch
__Time: 10:00 - 12:30, Saturday, July 26, 2014__   
__Location: St-Pie-de-Guire__

Will the rocket attain the correct altitude? Will the fragile payload be returned safely? Will the vehicle fly
straight and stable? Teams composed of graduate level ISU participants from all over the world will design and
launch model rockets that must meet a certain set of design criteria. They will design the airframes, select the motors, use computers to simulate the flight, and construct the rocket in a limited period of time. It is a real-world challenge and the team with the best performance will be recognized for their hard work. This is an event open to the public and visitors of all ages are welcome!

<img src="@root_path/public/images/ssp/events/rocket-1.jpg">
<img src="@root_path/public/images/ssp/events/rocket-2.jpg">
<img src="@root_path/public/images/ssp/events/rocket-3.jpg">

--------

### { Past events }

### Soffen Memorial Lecture
__Time: 19:00, Tuesday, June 17, 2014__  
__Location: ETS__

From the scientific legacy of the Apollo Program, we have learned that the surface of the Moon has an accessible record of the events in the first few hundred million years of the solar system.  We now know that both the Moon and the Earth were pummeled with a heavy bombardment of large bodies that reset the radioisotopic clocks in the lunar samples.  We know for the first time that the ancient crusts of the Earth and the Moon crystallized from magma oceans of planetary extent.  Age dates from the lunar samples, correlated with lunar surface crater populations, have provided a tool for estimating the timing of geologic events on all the terrestrial planets and have revealed intriguing correlations between episodes of bombardment and major historical events in the Earth’s biosphere.  A flood of data from an international fleet of spacecraft has given us new insights into the evolution of the lunar surface and has raised new puzzles related to current conditions.  New understanding gained from recent lunar exploration brings closer to reality the dream of permanent human presence on our satellite.

*Recorded on [Youtube](https://www.youtube.com/watch?v=_8xG4zKZcok)*


### Entrepreneurial Space Panel
__Time: 19:30, Monday, June 23, 2014__  
__Location: ETS__


Entrepreneurs are driving a new wave of growth in the space industry.  Leading entrepreneurs from the ISU community will discuss the ‘ins and outs’ starting a new company in the space industry and the lessons they have learned as they’ve worked to build their ventures.

*Recorded on [Youtube](https://www.youtube.com/watch?v=A1sRfE8sAtc)*


### Robotics Competition
__Time: 14:00, Friday, June 27, 2014__  
__Location: ETS__


Thanks to LEGO kits, talent, and a lot of imagination, ISU participants assisted by robotics experts will design and build autonomous robots following precise specifications to simulate planetary exploration. Robots will have to avoid obstacles and collect the most "gemstones" in the shortest given time. Their performance will be evaluated by a group of international experts. Visitors of all ages are welcome to share an educational and fun experience.

<img src="@root_path/public/images/ssp/events/robot-1.jpg">
<img src="@root_path/public/images/ssp/events/robot-2.jpg">
<img src="@root_path/public/images/ssp/events/robot-3.jpg">

*Recorded on [Youtube](https://www.youtube.com/watch?v=4RyRtVpZUio)*

### Evening Lecture - Current & Future Russian Space Programs 
__Time: 19:30, Monday, June 30, 2014__  
__Location: HEC__

The Russian space program has gone through a number of changes over the past five years and continues evolving. This lecture will describe these changes, cover the current status of the program and look forward at possible scenarios over the next decade

*Recorded on [Youtube](https://www.youtube.com/watch?v=ZI0Tgfjx3rs)*


### International Astronaut Panel
__Time: 19:30, Thursday, July 10, 2014__  
__Location: HEC Montreal, Côte-Sainte-Catherine Building, 3000 Côte-Sainte-Catherine Road Montréal, Quebec, H3T 2A7 Canada__  

Of the over 500 humans who have flown in space, only 57 have been female.  ISU is pleased to convene a unique panel of women space explorers, featuring astronauts who have flown on US space shuttles, Russian Soyuz, and Chinese Shenzhou vehicles to the International Space Station and the Chinese Tiangong-1.  Each panelist will share their unique experiences as they achieved their common goal of exploring space.

<img src="@root_path/public/images/ssp/events/astro-1.jpg">
<img src="@root_path/public/images/ssp/events/astro-2.jpg">
<img src="@root_path/public/images/ssp/events/astro-3.jpg">

*Recoreded on Youtube: <http://youtu.be/MG25gq23TWY>*


### Alumni Conference
__Dates: July 11 - 13, 2014__

The Alumni Conference brings an additional number of space professionals to the SSP to extend the personal network and knowledge.

During each SSP, ISU alumni and host partners organize an Alumni Conference with reunion events open to all ISU alumni and participants. It is a wonderful occasion to meet and share the experiences with the alumni who graduated from ISU over the past 25 years.

For SSP14, the alumni conference will be from July 10-13. Participants are welcome to join the alumni conference activities on Saturday July 12th including the Career Fair, Football Game, Poster Session and Space Masquerade.

More: <http://www.isunet.edu/alumni/alumni-conference>

<img src="@root_path/public/images/ssp/events/alum-1.jpg">
<img src="@root_path/public/images/ssp/events/alum-2.jpg">
<img src="@root_path/public/images/ssp/events/alum-3.jpg">


----------

#### { SSP14 Public Events Contact }

__Billy Jenck__   
Email: billy.jenck@isunet.edu    
Phone: +1 514-972-0381    