title: Exciting Progress Update of SSP14 Preparation
-
date: April 25, 2014
-
content: 
__By: David Kendall, SSP14 Director__

ISU is almost ready to welcome participants to the Space Studies Program 2014 in Montreal, Canada.  

On June 9, the Opening Ceremony will take place in the grand auditorium of the International Civil Aviation Organization building in downtown Montreal.  The two host institutions – École de Technologie Supérieur  and HEC Montréal – are providing strong support to ensure that this session will be an unforgettable experience for all who participate.  

All of the 57 core lectures are well in hand, the 23 multidisciplinary workshops have been finalised, the seven departments have each set up an exciting program of lectures and events, the four team project leaders are completing  their planning, and the program of evening lectures is being finalized.   Well over 100 leading space experts from around the world have already confirmed their involvement as faculty, lecturers and speakers.   

Highlights include a Heads of Space Agency panel; an international all-female astronaut panel; evening lectures by prominent astronauts from Canada, the US and Europe; presentations on current and future plans relating to lunar exploration, and the Russian and Chinese space programs; a robotics competition; and a model rocket launch activity.  

“New Space” will be highlighted this year with additional lectures, workshops and events highlighting this aspect of the space business, including a panel featuring ISU alumni who have started companies successfully participating in this new wave.  

For this summer’s session, team projects will focus on developing new approaches to the study of exoplanets, on-orbit servicing, open innovation, and public health delivery using space.  

Over 100 participants have already registered from almost 30 countries, however, there is still time to be part of what promises to be an extraordinary summer.  I personally look forward to welcoming all who will be part of this exciting program.  
